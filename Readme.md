### Diseño de archivos

### Recibos

Dominio
Recibo
Importe (2 decimales)
Libre
Fecha de Vencimientos AAAAMMDD
Libre
Codigo de Barras
Periodo
Libre

### Datos

Dominio
Titular
Calle
Altura
Piso 
Depto
Postal
Localidad

### Tareas a desarrollar

    - Deberán a partir del archivo de datos normalizar y generar las tablas (archivos planos ). Los archivos resultantes seran:

        - calles.txt
        - localidades.txt
    - Deberán crear un archivo datos.dat en donde se reemplace la calle string por el id, como así también desaparecera la localidad, cuya estructura sera algo similar:
        
        - Dominio (deberan guardarlo sin el - por ejemplo 314-HBG  se debe guardar 314HBG)
        - Titular
        - Id_Calle
        - Altura
        - Piso
        - Dpto
        - Postal

    - Realizar un formulario para realizar busquedas en el archivo datos.dat en donde se pueda buscar información por :

        - Dominio
        - Calle
        - Nombre del titular
    
    - Validar que contenga al menos un criterio indicado.
    - Se debe recibir el pedido de búsqueda, para luego realizar el codigo que permita encontrar lo que
    el usuario solicito y mostrarle una salida en pantalla que contenga una vista con el siguiente diseño:

        - Dominio
        - Titular
        - Calle (string)
        - Altura
        